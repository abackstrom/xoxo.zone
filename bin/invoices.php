<?php

// format invoices for the readme, suitable for copy/pasting

$opts = getopt('pl:');

$pdf_links = isset($opts['p']);
$limit = isset($opts['l']) ? $opts['l'] : 0;

$payments = explode("\n", trim(`doctl --context xoxo billing-history list --no-header --format Date,Type,Amount,InvoiceID | grep Invoice`));

$footer = '';

$count = 0;

foreach ($payments as $payment) {
    $count++;

    list($date, $type, $amount, $id) = preg_split("/\s+/", $payment);

    $date = strtotime($date) - 86400; // previous day
    $amount = abs((float)$amount);

    if ($pdf_links) {
        $pdf_links = sprintf('[pdf][%04d_%02d_pdf]', date('Y', $date), date('n', $date));
        $footer .= sprintf('  [%04d_%02d_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/%d.pdf' . PHP_EOL, date('Y', $date), date('n', $date), $id);
    } else {
        $pdf_links = '';
    }

    printf('| %-15s | %-38s | $%.2f |' . PHP_EOL, date('F Y', $date), $pdf_links, $amount);

    if ($limit && $count >= $limit) {
        break;
    }
}

if ($footer) {
    echo PHP_EOL . PHP_EOL . $footer;
}

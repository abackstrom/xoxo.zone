---
title: xoxo.zone README
layout: default.njk
---

[xoxo.zone](https://xoxo.zone/) is a community-run [Mastodon][1] server for the
[XOXO Festival][2] community. Find us in `#xoxo-mastodon` on the official Slack for
an instance invite!

## Administrators

[@annika][3] is the primary instance administrator, and is responsible for
monitoring, upgrades, and maintenance. The Andys and a handful of past operators
also retain admin privileges.

The XOXO [Code of Conduct][4] is enforced for all server members. Violations can
be reported to any admin through any means you feel comfortable with.

## Billing

The expected monthly bill for xoxo.zone approximately **$55** plus DigitalOcean
Spaces overage ($28 in December 2024).

- Hetzner AX41 (Ryzen 5 3600 6 core/12 thread, 64 GB RAM, 2TB RAID-1) @ €37.30 (~$40)
- DigitalOcean Spaces @ $5 + ~$13 storage overage + ~$10 bandwidth overage

It is not currently possible to for server members to financially contribute
to server costs.

[1]: https://joinmastodon.org/
[2]: https://xoxofest.com/
[3]: https://xoxo.zone/@annika
[4]: http://blog.xoxofest.com/conduct

### Invoice History

| Month          | Description         | Invoice                                | Cost                               |
| -------------- | ------------------- | -------------------------------------- | ---------------------------------- |
| November 2024  | Hetzner server      |                                        | <span title="€37.30">$39.23</span> |
| November 2024  | DigitalOcean Spaces | [pdf][2024_11_pdf]                     | $28.66                             |
| May 2024       |                     |                                        | $39.95                             |
| April 2024     |                     |                                        | $30.68                             |
| March 2024     |                     |                                        | $29.89                             |
| February 2024  |                     |                                        | $31.03                             |
| January 2024   |                     |                                        | $29.36                             |
| December 2023  |                     |                                        | $31.77                             |
| November 2023  |                     |                                        | $35.47                             |
| October 2023   |                     |                                        | $40.60                             |
| September 2023 |                     |                                        | $27.31                             |
| August 2023    |                     |                                        | $27.42                             |
| July 2023      |                     |                                        | $32.55                             |
| June 2023      |                     |                                        | $34.10                             |
| May 2023       |                     |                                        | $139.84                            |
| April 2023     |                     |                                        | $134.99                            |
| March 2023     |                     |                                        | $136.39                            |
| February 2023  |                     |                                        | $135.80                            |
| January 2023   |                     | [pdf][2023_01_pdf]                     | $140.17                            |
| December 2022  |                     | [pdf][2022_12_pdf]                     | $77.90                             |
| November 2022  |                     | [pdf][2022_11_pdf]                     | $67.53                             |
| October 2022   |                     | [pdf][2022_10_pdf]                     | $33.80                             |
| September 2022 |                     | [pdf][2022_09_pdf]                     | $33.80                             |
| August 2022    |                     | [pdf][2022_08_pdf]                     | $33.80                             |
| July 2022      |                     | [pdf][2022_07_pdf]                     | $33.80                             |
| June 2022      |                     | [pdf][2022_06_pdf]                     | $29.00                             |
| May 2022       |                     | [pdf][2022_05_pdf]                     | $29.00                             |
| April 2022     |                     | [pdf][2022_04_pdf]                     | $29.00                             |
| March 2022     |                     | [pdf][2022_03_pdf]                     | $29.00                             |
| February 2022  |                     |                                        | $30.77                             |
| January 2022   |                     |                                        | $47.18                             |
| December 2021  |                     |                                        | $43.84                             |
| November 2021  |                     |                                        | $40.67                             |
| October 2021   |                     |                                        | $37.54                             |
| September 2021 |                     |                                        | $34.38                             |
| August 2021    |                     |                                        | $31.26                             |
| July 2021      |                     |                                        | $29.07                             |
| June 2021      |                     |                                        | $29.00                             |
| May 2021       |                     |                                        | $34.61                             |
| April 2021     |                     |                                        | $37.11                             |
| March 2021     |                     |                                        | $34.27                             |
| February 2021  |                     |                                        | $31.57                             |
| January 2021   |                     |                                        | $29.29                             |
| December 2020  |                     |                                        | $29.00                             |
| November 2020  |                     |                                        | $29.00                             |
| October 2020   |                     |                                        | $33.04                             |
| September 2020 |                     |                                        | $33.31                             |
| August 2020    |                     |                                        | $30.81                             |
| July 2020      |                     |                                        | $29.06                             |
| June 2020      |                     |                                        | $29.00                             |
| May 2020       |                     |                                        | $29.00                             |
| April 2020     |                     |                                        | $29.00                             |
| March 2020     |                     |                                        | $29.00                             |
| February 2020  |                     | [csv][2020_02_csv], [pdf][2020_02_pdf] | $29.91                             |
| January 2020   |                     | [csv][2020_01_csv], [pdf][2020_01_pdf] | $30.28                             |
| December 2019  |                     | [csv][2019_12_csv], [pdf][2019_12_pdf] | $30.28                             |
| November 2019  |                     | [csv][2019_11_csv], [pdf][2019_11_pdf] | $29.73                             |
| October 2019   |                     | [csv][2019_10_csv], [pdf][2019_10_pdf] | $29.02                             |
| September 2019 |                     | [csv][2019_09_csv], [pdf][2019_09_pdf] | $30.09                             |
| August 2019    |                     | [csv][2019_08_csv], [pdf][2019_08_pdf] | $31.65                             |
| July 2019      |                     | [csv][2019_07_csv], [pdf][2019_07_pdf] | $36.03                             |
| June 2019      |                     | [csv][2019_06_csv], [pdf][2019_06_pdf] | $35.07                             |
| May 2019       |                     | [csv][2019_05_csv], [pdf][2019_05_pdf] | $33.80                             |
| April 2019     |                     | [csv][2019_04_csv], [pdf][2019_04_pdf] | $33.81                             |
| March 2019     |                     | [csv][2019_03_csv], [pdf][2019_03_pdf] | $33.28                             |
| February 2019  |                     | [csv][2019_02_csv], [pdf][2019_02_pdf] | $50.99                             |
| January 2019   |                     | [csv][2019_01_csv], [pdf][2019_01_pdf] | $35.97                             |
| December 2018  |                     | [csv][2018_12_csv], [pdf][2018_12_pdf] | $36.00                             |
| November 2018  |                     | [csv][2018_11_csv], [pdf][2018_11_pdf] | $35.89                             |
| October 2018   |                     | [csv][2018_10_csv], [pdf][2018_10_pdf] | $35.53                             |
| September 2018 |                     | [csv][2018_09_csv], [pdf][2018_09_pdf] | $39.70                             |
| August 2018    |                     | [csv][2018_08_csv], [pdf][2018_08_pdf] | $38.27                             |
| July 2018      |                     | [csv][2018_07_csv], [pdf][2018_07_pdf] | $31.38                             |
| June 2018      |                     | [csv][2018_06_csv], [pdf][2018_06_pdf] | $31.55                             |
| May 2018       |                     | [csv][2018_05_csv], [pdf][2018_05_pdf] | $38.61                             |
| April 2018     |                     | [csv][2018_04_csv], [pdf][2018_04_pdf] | $31.15                             |
| March 2018     |                     | [csv][2018_03_csv], [pdf][2018_03_pdf] | $29.70                             |

\_Figures are a little muddy up until May 2018, when the resources were moved
to their own Digital Ocean Team.

[2024_11_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/499268040.pdf
[2023_01_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/451992638.pdf
[2022_12_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/449986926.pdf
[2022_11_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/448673939.pdf
[2022_10_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/446809514.pdf
[2022_09_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/445138507.pdf
[2022_08_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/443970740.pdf
[2022_07_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/442309199.pdf
[2022_06_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/440636466.pdf
[2022_05_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/439383199.pdf
[2022_04_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/437995454.pdf
[2022_03_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/436583368.pdf
[2020_02_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/414064739.pdf
[2020_02_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/414064739.csv
[2020_01_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/413609826.pdf
[2020_01_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/413609826.csv
[2019_12_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/413038270.pdf
[2019_12_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/413038270.csv
[2019_11_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/412299758.pdf
[2019_11_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/412299758.csv
[2019_10_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/411869136.pdf
[2019_10_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/411869136.csv
[2019_09_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/411292017.pdf
[2019_09_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/411292017.csv
[2019_08_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/410537800.pdf
[2019_08_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/410537800.csv
[2019_07_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/409622494.pdf
[2019_07_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/409622494.csv
[2019_06_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/409155099.pdf
[2019_06_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/409155099.csv
[2019_05_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/408875610.pdf
[2019_05_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/408875610.csv
[2019_04_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/407993288.pdf
[2019_04_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/407993288.csv
[2019_03_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/407381995.pdf
[2019_03_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/407381995.csv
[2019_02_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/406781928.pdf
[2019_02_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/406781928.csv
[2019_01_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/406203174.pdf
[2019_01_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/406203174.csv
[2018_12_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/390783533.pdf
[2018_12_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/390783533.csv
[2018_11_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/374296901.pdf
[2018_11_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/374296901.csv
[2018_10_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/357898669.pdf
[2018_10_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/357898669.csv
[2018_09_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/341015755.pdf
[2018_09_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/341015755.csv
[2018_08_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/326075625.pdf
[2018_08_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/326075625.csv
[2018_07_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/309729603.csv
[2018_07_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/309729603.pdf
[2018_06_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/293033268.csv
[2018_06_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/293033268.pdf
[2018_05_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/277695949.csv
[2018_05_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/277695949.pdf
[2018_04_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/264645538.csv
[2018_04_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/264645538.pdf
[2018_03_csv]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/249688731.csv
[2018_03_pdf]: https://xoxo-zone.nyc3.digitaloceanspaces.com/billing/249688731.pdf

## Instance history

Updates are usually tagged with [#xoxozone][5] on Mastodon.

- 2024-01-16 -- Mastodon upgraded from v4.3.2 to v4.3.3
- 2024-12-03 -- Mastodon upgraded from v4.3.1 to v4.3.2
- 2024-10-21 -- Mastodon upgraded from v4.3.0 to v4.3.1
- 2024-10-19 -- Mastodon upgraded from v4.2.13 to v4.3.0
- 2024-10-01 -- Mastodon updated from v4.2.12 to v4.2.13
- 2024-08-19 -- Mastodon updated from v4.2.11 to v4.2.12
- 2024-08-18 -- Mastodon updated from v4.2.10 to v4.2.11
- 2024-05-31 -- Mastodon updated from v4.2.8 to v4.2.9
- 2024-03-20 -- Mastodon updated from v4.2.7 to v4.2.8
- 2024-02-16 -- Mastodon updated from v4.2.6 to v4.2.7
- 2024-02-14 -- Mastodon updated from v4.2.5 to v4.2.6
- 2024-02-01 -- Mastodon updated from v4.2.3 to v4.2.5
- 2023-12-15 -- Mastodon updated from v4.2.1 to v4.2.3
- 2023-10-26 -- Mastodon updated from v4.2.0 to v4.2.1
- 2023-09-24 -- Mastodon updated from v4.1.8 to v4.2.0
- 2023-09-19 -- Mastodon updated from v4.1.7 to v4.1.8
- 2023-09-07 -- Mastodon updated from v4.1.6 to v4.1.7
- 2023-07-31 -- Mastodon updated from v4.1.4 to v4.1.6
- 2023-05-24 -- Server migrated from DigitalOcean to Hetzner [#][hist42]
- 2023-04-11 -- Mastodon updated from v4.1.0 to v4.1.2 [#][hist41]
- 2023-02-27 -- Mastodon updated from v4.0.2 to v4.1.0 [#][hist40]
- 2022-11-19 -- Mastodon updated from v3.5.5 to v4.0.2 [#][hist39]
- 2022-11-15 -- Mastodon updated from v3.5.4 to v3.5.5 [#][hist38]
- 2022-11-14 -- Mastodon updated from v3.5.3 to v3.5.4 [#][hist37]
- 2022-11-07 -- Server upgraded from 2 vCPU, 4 GB RAM to 4 vCPU, 8 GB RAM [#][hist36]
- 2022-05-27 -- Mastodon updated from v3.5.2 to v3.5.3 [#][hist35]
- 2022-05-10 -- Mastodon updated from v3.5.1 to v3.5.2 [#][hist34]
- 2022-04-25 -- Mastodon updated from v3.5.0 to v3.5.1 [#][hist33]
- 2022-04-02 -- Mastodon updated from v3.4.6 to v3.5.0 [#][hist32]
- 2022-02-03 -- Mastodon updated from v3.4.5 to v3.4.6 [#][hist31]
- 2022-02-03 -- Mastodon updated from v3.4.4 to v3.4.5 [#][hist30]
- 2021-12-01 -- Mastodon updated from v3.4.3 to v3.4.4 [#][hist29]
- 2021-11-23 -- Mastodon updated from v3.4.1 to v3.4.3 [#][hist28]
- 2021-06-03 -- Mastodon updated from v3.4.0 to v3.4.1 [#][hist27]
- 2021-05-17 -- Mastodon updated from v3.3.0 to v3.4.0 [#][hist26]
- 2021-01-31 -- Mastodon updated from v3.2.1 to v3.3.0 [#][hist25]
- 2020-11-18 -- Mastodon updated from v3.2.0 to v3.2.1 [#][hist24]
- 2020-07-27 -- Mastodon updated from v3.1.4 to v3.2.0 [#][hist23]
- 2020-05-23 -- Mastodon updated from v3.1.2 to v3.1.4 [#][hist22]
- 2020-02-27 -- Mastodon updated from v3.0.1 to v3.1.2 [#][hist21]
- 2019-11-14 -- Mastodon updated from v2.9.3 to v3.0.1 [#][hist20]
- 2019-08-24 -- Mastodon updated from v2.9.2 to v2.9.3 [#][hist19]
- 2019-07-29 -- Migrated media from local storage to DigitalOcean Spaces [#][hist18]
- 2019-06-22 -- Mastodon updated from v2.9.1 to v2.9.2 [#][hist17]
- 2019-06-22 -- Mastodon updated from v2.9.0 to v2.9.1 [#][hist16]
- 2019-06-14 -- Mastodon updated from v2.8.2 to v2.9.0 [#][hist15]
- 2019-05-09 -- Mastodon updated from v2.8.0 to v2.8.2 [#][hist14]
- 2019-04-15 -- Mastodon updated from v2.7.3 to v2.8.0 [#][hist13]
- 2019-02-24 -- Mastodon updated from v2.7.1 to v2.7.3 [#][hist12]
- 2019-02-01 -- Mastodon updated from v2.6.5 to v2.7.1 [#][hist11]
- 2019-02-01 -- Migration to new Ubuntu 18.04.1 host [#][hist11]
- 2018-12-02 -- Mastodon updated from v2.5.2 to v2.6.5 [#][hist10]
- 2018-10-28 -- Mastodon updated from v2.5.0 to v2.5.2 [#][hist9]
- 2018-09-21 -- Mastodon updated from v2.4.5 to v2.5.0 [#][hist8]
- 2018-09-21 -- Media volume ran out of inodes; created new volume [#][hist7]
- 2018-08-28 -- Mastodon updated from v2.4.4 to v2.4.5 [#][hist6]
- 2018-08-22 -- Mastodon updated from v2.4.3 to v2.4.4 [#][hist5]
- 2018-08-17 -- Mastodon updated from v2.4.2 to v2.4.3; Media volume shrunk from 64GB to 32GB [#][hist4]
- 2018-07-03 -- Mastodon updated from v2.4.0 to v2.4.2 [#][hist3]
- 2018-05-23 -- Mastodon updated from v2.2.0 to v2.4.0 [#][hist2]
- 2018-05-22 -- Server downgraded from Ubuntu 16.10 to Ubuntu 16.04 LTS [#][hist1]
- 2018-02-27 -- Server admin passed from [@davidcelis][6] to [@annika][3]

  [hist1]: https://xoxo.zone/@annika/100076350401843082
  [hist2]: https://xoxo.zone/@annika/100081086157490657
  [hist3]: https://xoxo.zone/@annika/100314038839259607
  [hist4]: https://xoxo.zone/@annika/100565424786529315
  [hist5]: https://xoxo.zone/@annika/100595985129380881
  [hist6]: https://xoxo.zone/@annika/100627621424865681
  [hist7]: https://xoxo.zone/@annika/100762079405088089
  [hist8]: https://xoxo.zone/@annika/100775934532048510
  [hist9]: https://xoxo.zone/@annika/100974297084088580
  [hist10]: https://xoxo.zone/@annika/101174308043039365
  [hist11]: https://xoxo.zone/@annika/101520682840128816
  [hist12]: https://xoxo.zone/@annika/101648052920872029
  [hist13]: https://xoxo.zone/@annika/101933601984394780
  [hist14]: https://xoxo.zone/@annika/102069514922897221
  [hist15]: https://xoxo.zone/@annika/102273432762615030
  [hist16]: https://xoxo.zone/@annika/102315433210808566
  [hist17]: https://xoxo.zone/@annika/102316651028335056
  [hist18]: https://xoxo.zone/@annika/102524374045241997
  [hist19]: https://xoxo.zone/@annika/102672650972278607
  [hist20]: https://xoxo.zone/@annika/103139548391122692
  [hist21]: https://xoxo.zone/@annika/103731439457527487
  [hist22]: https://xoxo.zone/@annika/104218453792663950
  [hist23]: https://xoxo.zone/@annika/104588546439248464
  [hist24]: https://xoxo.zone/@annika/105234323392079949
  [hist25]: https://xoxo.zone/@annika/105651972842502052
  [hist26]: https://xoxo.zone/@annika/106249133757727664
  [hist27]: https://xoxo.zone/@annika/106345543544084977
  [hist28]: https://xoxo.zone/@annika/107326130736536147
  [hist29]: https://xoxo.zone/@annika/107373443723062420
  [hist30]: https://xoxo.zone/@annika/107732822932681426
  [hist31]: https://xoxo.zone/@annika/107735507927307001
  [hist32]: https://xoxo.zone/@annika/108063965317154245
  [hist33]: https://xoxo.zone/@annika/108194651166567729
  [hist34]: https://xoxo.zone/@annika/108279805968549653
  [hist35]: https://xoxo.zone/@annika/108372281409670262
  [hist36]: https://xoxo.zone/@annika/109303703891785619
  [hist37]: https://xoxo.zone/@annika/109344091367843994
  [hist38]: https://xoxo.zone/@annika/109349544070146776
  [hist39]: https://xoxo.zone/@annika/109370811217908971
  [hist40]: https://xoxo.zone/@annika/109936015736981196
  [hist41]: https://xoxo.zone/@annika/110181431437409561
  [hist42]: https://xoxo.zone/@annika/110419363948675343
  [5]: https://xoxo.zone/web/timelines/tag/xoxozone
  [6]: https://xoxo.zone/@davidcelis

## Miscellaneous

- DNS is managed by Andy Baio
- [Uptime Robot][7] is used for downtime notifications (5 min polling)
- New Relic is used to track server metrics

  [7]: https://uptimerobot.com/

## Graphs

![CPU Usage](https://gorgon.nr-assets.net/image/4294f2c5-97a4-4da3-8a25-e0e1fb99842b?type=line)

![Sidekiq Job Latency](https://gorgon.nr-assets.net/image/af5758fe-344b-45f2-b6d4-d3c9a5e791f2?type=line)

---

[permalink](https://xoxo.zone/docs/), [source](https://gitlab.com/xoxo.zone/docs/)

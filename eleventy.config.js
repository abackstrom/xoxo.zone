import markdownIt from 'markdown-it';
import markdownItAttrs from 'markdown-it-attrs';

export default function(eleventyConfig) {
    const markdownItOptions = {
    html: true,
    breaks: true,
    linkify: true
    };

    const markdownLib = markdownIt(markdownItOptions).use(markdownItAttrs)
    eleventyConfig.setLibrary('md', markdownLib);
	eleventyConfig.addWatchTarget("./main.css");
};

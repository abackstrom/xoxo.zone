---
title: Migration guide
layout: default.njk
---

Guide for migrating Mastodon to a new host.

See the official docs for [Migrating to a new machine][1].

**Note:** hostnames are usually in the format `xoxo-#`, incrementing by 1
for each new host. You will need the short hostname for Ansible. These docs
will use `xoxo-6` in examples

## Pre-requisites

Check out the `xoxo-scripts` repository:

```
git clone git@gitlab.com:xoxo.zone/xoxo-scripts.git
```

Check out the `xoxo-passwords` repository:

```
git clone git@gitlab.com:xoxo.zone/xoxo-passwords.git
```

On an existing host, create a Restic password. Save this for later.

```
restic key add --host <hostname> --user xoxobackup
```

In the [DigitalOcean cloud dashboard][2], create a new Spaces Key. Use the
name format `restic@xoxo-6`. Save the key and secret for later.

## Set up and run Ansible on the new host

In `xoxo-scripts`, add a new line to the `hosts` file. Use full hostname. If
the incrementing, permanent hostname is not available, use the short hostname
with `ansible_host`:

```yaml
[mastodon]
xoxo-6 ansible_host=10.0.0.1
```

Create a new Restic variables file for the host. Note that `ansible-vault`
always uses your `$EDITOR` setting. You should always edit the file using
`ansible-vault edit` to reduce the risk of committing an unencrypted file.

```
ansible-vault edit host_vars/xoxo-6/restic.yml
```

Add `restic_s3_key`, `restic_s3_secret`, and `restic_pass` to this file.
Substitute in the secrets you created in the pre-requisites section.

```yaml
---
restic_s3_key: "SPACES_KEY_HERE"
restic_s3_secret: "SPACES_SECRET_HERE"
restic_pass: "RESTIC_PASS_HERE"
```

Commit and push your changes to `xoxo-scripts`. You may then run the playbook:

```
./run
```

## WIP: Set up new host

_This section is unfinished._

Do the new host setup, stopping at TKTK.

### Old Host: Stop Mastodon and take a backup

Stop Mastodon services on the old host:

```
mastodon-bounce stop all
```

Disable services so they don't automatically start if you reboot:

```
mastodon-bounce disable all
```

Run the backup on the old host:

```
sudo -u xoxobackup restic-backup
```

### New host: Restore from backup

Restore the latest snapshot:

```
eval $(sudo cat ~xoxobackup/.restic)
restic restore latest --target restore
```

- install env file
- restore postgres db
- restore redis db

[1]: https://docs.joinmastodon.org/admin/migrating/
[2]: https://cloud.digitalocean.com/account/api/spaces?i=10427b

---
title: Server Branding and Policies
layout: default.njk
---

This document is manually kept in sync with the server settings.

# Server Description

<blockquote>

A community space for attendees and speakers of the XOXO Festival, held in Portland, Oregon. This Mastodon instance is community-run and is not affiliated with the past organisers of XOXO Festival.

</blockquote>


# About

<blockquote>

# About

A Mastodon instance for the XOXO community. Started in 2012, [XOXO](https://xoxofest.com/) was an experimental festival for independent artists who live and work online.

This instance is community-run and is not affiliated with the organisers of XOXO Festival.

# Code of Conduct

Everyone participating in the XOXO community—including, but not limited to the XOXO festival, Slack team, and Mastodon instance—is required to agree to the [Code of Conduct](https://xoxofest.com/conduct). This includes all attendees, speakers, performers, patrons (sponsors), volunteers, and staff.

XOXO is dedicated to providing a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, mental illness, neurotype, physical appearance, body, age, race, ethnicity, nationality, language, or religion. We do not tolerate harassment of participants in any form.

Anyone who violates this Code of Conduct may be sanctioned or expelled from these spaces at the discretion of the server moderators.

Anyone expelled from xoxo.zone will be given seven days to export their data, migrate to another instance, or delete their account, a period during which their account will be hidden from public view. After that period, their account will be permanently deleted.

</blockquote>

# Registrations Closed Message

<blockquote>

<p>New user registration on xoxo.zone is limited to the XOXO community. Come find us in #xoxo-mastodon in the official Slack for an invite!</p>

</blockquote>

# Rules

<blockquote>

No harassment. For a detailed definition, refer to the XOXO Code of Conduct at xoxofest.com/conduct.

No hate speech, including but not limited to sexist, racist, homophobic, and transphobic posts.

No illegal content or activity.

Use content warnings for adult content, or posts that may be disturbing or triggering.

No sharing personal information about others without their consent.

No intentional sharing of false or misleading information.

No spam. Self-promotion is welcome, sending mass unsolicited replies is not.

</blockquote>

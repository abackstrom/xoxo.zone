---
title: xoxo.zone docs
layout: default.njk
---

This is a minimal docs repository for the xoxo.zone Mastodon server.

| Property   | Value                                 |
| ---------- | ------------------------------------- |
| URL        | <https://xoxo.zone/docs/>               |
| Repository | [xoxo.zone/docs][1]                   |
| Checkout   | `/var/www/xoxo-docs`                  |
| nginx      | `/etc/nginx/sites-available/mastodon` |

The `mastodon` user on the server pulls this repository and runs an npm build
every 5 minutes. This is configured via the user cron.

The main nginx configuration file includes a directory alias for the docs `output` directory.

[1]: https://gitlab.com/xoxo.zone/docs
